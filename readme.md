##Dependencies
- Tesseract https://github.com/UB-Mannheim/tesseract/wiki
-- Might work without binary because package might be able to compile it
- ImageMagick http://imagemagick.org/script/download.php
-- Might work without binary because package might be able to compile it
- NodeJS https://nodejs.org/en/download/

##Tools
- VietOCR.NET-4.3_64 https://sourceforge.net/projects/vietocr/
-- This allows for testing against Tesseract OCR without running the app
- http://vietocr.sourceforge.net/training.html Can be used for testing out custom training files (NOT NEEDED)

##Possible Tools
- Capture2Text http://capture2text.sourceforge.net/
-- This might be a replacement for Tesseract
- futura.traineddata
-- Generated from http://trainyourtesseract.com/
-- This might be able to be used to train Tesseract better against the font. 

##Install Steps
1) Install dependencies
2) Run npm install

##Running
1) Run npm start 


https://github.com/peterbraden/node-opencv/issues/387
http://docs.opencv.org/2.4/modules/imgproc/doc/object_detection.html?highlight=matchtemplate#matchtemplate
https://github.com/peterbraden/node-opencv
https://github.com/strarsis/node-opencv-templatematching-test

cv.readImage("./test.jpg", function(err, im) {
    im.convertGrayscale();
    im.matchTemplate("./template.jpg", 3, function(result){
        result.save('./pic.jpg');
    });
});

Thanks for guidance. I have successfully install node-opencv over my windows 10
following is my walk-through to install this

Download Install opencv @ - (I used version 2.4.4)
http://opencv.org/downloads.html
Put it in c:\opencv
Install python version 2.7 @
http://www.python.org/download/releases/2.7/
put it in c:\python27
install pkg-config by downloading the all in one bundle @ - (I used Gtk+ 3.6.4)
http://www.gtk.org/download/win64.php
put it in c:\pkg-config
Add the following to your path variables
C:\pkg-config\bin;C:\OpenCV\build\x64\vc11\bin;
Install visual-studio in 4 steps
First install Visual C++ 2010 Express
Second install Windows SDK for windows 7 and .net framework 4
Third install Visual Studio 2010 Service Pack 1
Fourth install Visual C++ 2010 Service Pack 1 Compiler
Download npeterbraden/node-opencv fork
git clone https://github.com/peterbraden/node-opencv
edit file src/Matrix.cpp
put "inline double round( double d ) { return floor( d + 0.5);}" below "cv::Rect* setRect(Local objRect, cv::Rect &result);"
run npm install

http://sourceforge.net/projects/pkgconfiglite/files/
http://stackoverflow.com/questions/1710922/how-to-install-pkg-config-in-windows
https://prateekvjoshi.com/2013/10/18/package-opencv-not-found-lets-find-it/

http://answers.opencv.org/question/28052/pkg-config-for-windows-7/
http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html


NOTES
COPY node-opencv-master/utils/opencv_x64.pc to your pkg-config directory
https://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.9/opencv-2.4.9.exe/download

##INSTALL

Download OpenCV 2.4.9

- https://sourceforge.net/projects/opencvlibrary/files/opencv-win/2.4.9/opencv-2.4.9.exe/download

- Once downloaded edit /opencv/build/include/opencv2/flann/any.h to change all references of 'std::type_info' to 'type_info'

Download Package Config

- http://sourceforge.net/projects/pkgconfiglite/files/

Adjust PATH

C:\pkg-config\bin;C:\OpenCV249\opencv\build\x64\vc12\bin\;C:\OpenCV249\opencv\build\x64\vc12\lib;

- Copy util/opencv_x64.pc from GIT opencv repo into a path reachable directory for pkg-config to use and rename it to opencv