var cv = require('opencv');

// B, G, R
var COLORS = [
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],
    [51, 102, 255],

    [102, 51, 255],
    [204, 51, 255],
    [255, 51, 204],
    [51, 204, 255],
    [0, 61, 245],
    [0, 46, 184],
    [255, 51, 102],
    [51, 255, 204],
    [184, 138, 0],
    [245, 184, 0],
    [255, 102, 51],
    [51, 255, 102],
    [102, 255, 51],
    [204, 255, 51],
    [255, 204, 51],
];

COLORS.forEach(function(rgb, i) {
    COLORS[i] = [rgb[2], rgb[1], rgb[0]];
});

function parseScreenshot(file, output, debugOutput) {
    var TM_CCORR_NORMED = 3,
        templatesToMatch = [
            ['./matches/pharah2.jpg', 0.79, 1080],
            ['./matches/zenyatta.jpg', 0.83, 1080],

            ['./matches/lucio2.jpg', 0.81, 1080],
            // ['./matches/zenyatta.jpg', .74, 1080],
            ['./matches/ana.jpg', 0.85, 1080],
            ['./matches/bastion.jpg', 0.80, 1080],
            // // Adjusted
            ['./matches/dva.jpg', 0.80, 1080],
            ['./matches/hanzo.jpg', 0.80, 1080],
            ['./matches/junkrat.jpg', 0.80, 1080],
            // Adjusted but doesn't find mcbean
            ['./matches/mccree.jpg', 0.80, 1080],
            ['./matches/mei.jpg', 0.80, 1080],
            //    // Adjusted
            ['./matches/mercy.jpg', 0.81, 1080],
            // ['./matches/pharah.jpg', 0.80, 1080],
            ['./matches/reaper.jpg', 0.80, 1080],
            //    // Adjusted but not finding X
            ['./matches/reinhardt.jpg', 0.79, 1080],
            //    // Partially Adjusted
            ['./matches/roadhog.jpg', 0.77, 1080],
            //    // Adjusted
            ['./matches/soldier76.jpg', 0.86, 1080],
            ['./matches/sombra.jpg', 0.80, 1080],
            ['./matches/symmetra.jpg', 0.80, 1080],
            ['./matches/torbjorn.jpg', 0.87, 1080],
            //    // Adjusted
            ['./matches/tracer.jpg', 0.80, 1080],
            ['./matches/widowmaker.jpg', 0.80, 1080],
            ['./matches/winston.jpg', 0.80, 1080],
            ['./matches/zarya.jpg', 0.80, 1080],
            // Partially Adjusted
            ['./matches/genji.jpg', 0.85, 1080],
            ['./matches/0.jpg', 0.95],
            ['./matches/1.jpg', 0.93],
            ['./matches/2.jpg', 0.93],
            ['./matches/3.jpg', 0.93],
            ['./matches/4.jpg', 0.93],
            ['./matches/5.jpg', 0.93],
            ['./matches/6.jpg', 0.93],
            ['./matches/7.jpg', 0.93],
            ['./matches/8.jpg', 0.93],
            ['./matches/9.jpg', 0.93],
        ];

    cv.readImage(file, function(err, im) {
        var target = im.copy(),
            targetMask;

        target.convertGrayscale();
        targetMask = target.copy();

        var results = {};

        templatesToMatch.forEach(function(templateDetails, i) {
            cv.readImage(templateDetails[0], function(err, template) {
                var keys = [],
                    res,
                    key,
                    minMax,
                    scaleFactor = parseFloat((im.height() / (templateDetails[2] || 720)).toFixed(2)),
                    width = scaleFactor * template.width(),
                    height = scaleFactor * template.height(),
                    templateCopy,
                    itemColor = [Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255)];
                console.log(file.split('/').pop(), templateDetails[0], scaleFactor, template.width(), template.height(), itemColor);
                template.resize(width, height);
                templateCopy = template.copy();
                templateCopy.convertGrayscale();

                // console.log(im.height(), templateDetails[2], im.height() / templateDetails[2]);
                while (true) {
                    res = targetMask.matchTemplateByMatrix(templateCopy, TM_CCORR_NORMED);
                    minMax = res.minMaxLoc();
                    key = minMax.maxLoc.x + 'x' + minMax.maxLoc.y;

                    if (minMax.maxVal < templateDetails[1] || results[key]) {
                        console.log('BLOCKED', key, templateDetails[1], minMax.maxVal);
                        break;
                    }

                    keys.push(key);

                    console.log(templateDetails[0], key, minMax.maxVal);

                    results[key] = res;
                    targetMask.rectangle([minMax.maxLoc.x, minMax.maxLoc.y], [width, height], [0, 0, 0], -1);
                    im.rectangle([minMax.maxLoc.x, minMax.maxLoc.y], [width, height], itemColor, 2);
                    im.save(output);
                }

                keys.forEach(function(key) {
                    var result = results[key],
                        minMax = result.minMaxLoc();
                    targetMask.rectangle([minMax.maxLoc.x, minMax.maxLoc.y], [width, height], [255, 255, 255]);

                    template.copyTo(target, minMax.maxLoc.x, minMax.maxLoc.y, width, height);
                    template.copyTo(im, minMax.maxLoc.x, minMax.maxLoc.y, width, height);
                });

                im.save(output);
                if (debugOutput) {
                    target.save(debugOutput);
                    targetMask.save(debugOutput);
                }

                console.log('---------');
            });
        });

        // im.save(output);
        // var img_crop = im.crop(res[1], res[2], res[3], res[4]);
        // console.log('img_crop', img_crop);
        // img_crop.save('./crop.png');
    });
}

// parseScreenshot("./images/car1.jpg", './output.jpg');
// parseScreenshot("./images/vlcsnap-error277.jpg", 'vlcsnap-error277.jpg', 'vlcsnap-error277-debug.jpg', .65);
parseScreenshot("./images/vlcsnap-error294.png", './vlcsnap-error294.jpg', './vlcsnap-error294-debug.jpg');
// parseScreenshot("./images/vlcsnap-error559.png", 'vlcsnap-error559.jpg', 'vlcsnap-error559-debug.jpg');
