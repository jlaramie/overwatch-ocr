var tesseract = require('node-tesseract');
var easyimage = require('easyimage');
var exec = require('child_process').exec;
var fs = require('fs');
var tmpdir = require('os').tmpdir(); // let the os take care of removing zombie tmp files
var uuid = require('node-uuid');
var path = require('path');
var glob = require("glob");

// vlcsnap-error559.png
// convert images\vlcsnap-error559.png -gravity South -crop 100%x18%! output.png
// convert output.png -gravity North -crop 100%x65% output2.png
// 

// tesseract.process(__dirname + '/test.png', {
// 	config: 'config.txt',
// 	psm: 3
// }, function(err, text) {
//     if(err) {
//         console.error(err);
//     } else {
//         console.log(text);
//     }
// });
// 
var name = 'vlcsnap-error294.png';
	name = 'vlcsnap-error559.png';
easyimage.exec([
    'convert',
    `images/${name}`,
    '-gravity South -crop 100%x23%!',
    '-gravity North -crop 100%x70%!',
    // '-monochrome',
    '-scale 200%',
    // '-type grayscale',
    // '-contrast',
    // '-contrast',
    // // '-blur 1x65535 -blur 1x65535 -blur 1x65535',
    // '-normalize',
    // '-despeckle',
    // '-despeckle',
    // '-despeckle',
    // '-threshold 50%',
    '-sharpen 1',
    // '-negate',
    // '-gamma 100',
    // '-threshold 50%',
    // '-posterize 3',
    // '-black-threshold 10%',
    // '-white-threshold 90%',
    // composite -compose src-out mask.png input.jpg -matte output.jpg
    // '-alpha off -compose copy_opacity -composite stats-mask2.png',
    // '-alpha on -compose copy_opacity -composite stats-mask.png',
    // '-flatten',
    // '-gravity South -fill red -draw "rectangle 0%,50% 100%,45%"',
    // '-edge',
    // '-sharpen 5',
    // '-morphology close diamond',
    // '-morphology Hit-and-Miss "1x8:1,0,1,1,0,0,0,0"',
    `output-${name}`,
].join(' ')).then(function() {
	easyimage.exec([
    	'composite',
    	'-compose',
    	'CopyOpacity',
    	'stats-mask3.png',
    	`output-${name}`,
    	`output-${name}`,
	].join(' ')).then(function() {
	// 	easyimage.exec([
	//     	'convert',
	//     	`output-${name}`,
	//     	// '-matte',
	//     	// '-fill black',
	//     	// '-flatten',
	//     	`-crop 400x78`,
	//     	`parts/output-%02d-${name}`,
	// 	].join(' ')).then(function() {
	// 		fs
	// 	        .readdirSync(path.join(__dirname, 'parts'))
	// 	        .filter(function(file) {
	// 	            return true;
	// 	        })
	// 	        .forEach(function(file) {
	// 	            tesseract.process(__dirname + `/parts/${file}`, {
	// 			        config: 'config.txt',
	// 			        l: 'eng+futura',
	// 			        psm: 7
	// 			    }, function(err, text) {
	// 			        if (err) {
	// 			            console.error(err);
	// 			        } else {
	// 			            console.log(file, ':', text);
	// 			        }
	// 			    });
	// 	        });

		    tesseract.process(__dirname + `/output-${name}`, {
		        config: 'config.txt',
		        l: 'eng+futura',
		        psm: 3
		    }, function(err, text) {
		        if (err) {
		            console.error(err);
		        } else {
		            console.log(text);
		        }
		    });
		// });
	});
});

//  easyimage.exec([
// 	'convert',
// 	'images/vlcsnap-error559.png',
// 	'-gravity South -crop 100%x23%!',
// 	'-gravity North -crop 100%x70%!',
// 	// '-monochrome',
// 	'-scale 200%',
// 	// '-type grayscale',
// 	// '-contrast',
// 	'-contrast',
// 	// '-negate',
// 	// // '-blur 1x65535 -blur 1x65535 -blur 1x65535',
// 	// '-normalize',
// 	// '-despeckle',
// 	// '-despeckle',
// 	// '-despeckle',
// 	// '-threshold 50%',
// 	// // '-sharpen 1',
// 	// '-posterize 3',
// 	// '-gamma 100',
// 	// '-black-threshold 10%',
// 	// '-edge',
// 	// '-sharpen 5',
// 	// '-morphology close diamond',
// 	// '-morphology Hit-and-Miss "1x8:1,0,1,1,0,0,0,0"',
// 	'output-559.png'
// ].join(' ')).then(function() {
// 	tesseract.process(__dirname + '/output-559.png', {
// 		config: 'config.txt',
// 		l: 'eng+futura',
// 		psm: 3
// 	}, function(err, text) {
// 	    if(err) {
// 	        console.error(err);
// 	    } else {
// 	        console.log(text);
// 	    }
// 	});
// });

// tesseract.process(__dirname + '/images/test.png', {
// 	config: 'config.txt',
// 	psm: 3
// }, function(err, text) {
//     if(err) {
//         console.error(err);
//     } else {
//         console.log(text);
//     }
// });
// 
// applybox_exposure_pattern
